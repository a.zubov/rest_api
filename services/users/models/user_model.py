from pydantic import BaseModel, field_validator


class UserModel(BaseModel):
    email: str
    uuid: str
    name: str
    nickname: str

    @field_validator('email', 'uuid', 'name', 'nickname')
    def fields_not_empty(cls, value):
        if not value:
            raise ValueError('Fields are required')
        return value
