import allure
import pytest

from config.base_test import BaseTest


@allure.epic('Administration')
@allure.feature('Users')
class TestUsers(BaseTest):
    @pytest.mark.regression
    @allure.title('Create a user')
    def test_create_user(self):
        user = self.api_users.create_user()
        print(self.api_users.get_user_by_id(user.uuid))

